CC ?= gcc
RM ?= rm -f
CFLAGS ?= -O3 -g -Wall -Wextra -Wno-unused-result
SECURITY_CFLAGS ?= -Wl,-z,relro,-z,now -D_FORTIFY_SOURCE=2 -fstack-protector-all
REQUIRED_CFLAGS = -std=c11 -O3 -fPIC -ldl -pthread -shared -Wl,-z,initfirst
BINARY = libtraceheap.so

lib%.so: %.c Makefile
	$(CC) $(CFLAGS) $(SECURITY_CFLAGS) $(REQUIRED_CFLAGS) $< -o $@

.PHONY: all clean
all: $(BINARY)
clean:
	$(RM) $(BINARY)
