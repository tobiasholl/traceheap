/*
 * This library, when included in LD_PRELOAD, traces any memory allocation calls
 * performed by the glibc through any of the standard means, and the brk() and
 * sbrk() functions. Note that we cannot intercept memory allocated directly
 * through the brk and sbrk system calls.
 *
 *   Copyright (c) 2019 - Tobias Holl
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *   MA 02110-1301 USA
 *
 * For additional functionality, see https://gitlab.com/tobiasholl/ldmalloc
 *
 * To compile, run make, or your local equivalent of
 *
 *   gcc -std=c11 -fPIC -ldl -pthread -shared -Wl,-z,initfirst \
 *       traceheap.c -o libtraceheap.so
 *
 * You can also define TRACEHEAP_NO_PTHREAD and remove the -pthread flag
 * to disable this dependency, but I cannot guarantee functionality in a
 * multithreaded environment in this mode.
 *
 * To use the compiled version, simply add it to LD_PRELOAD.
 *
 *   $ LD_PRELOAD=/path/to/libtraceheap.so ./binary
 *
 * To enable tracing for binaries running in GDB, use
 *
 *   $ set exec-wrapper env "LD_PRELOAD=libtraceheap.so"
 *
 * Configurable settings:
 *    TRACEHEAP_LOG_FD                   File descriptor to log to
 *                                       (default: STDOUT_FILENO)
 *    TRACEHEAP_EMERGENCY_CALLOC_SPACE   Space for calls to calloc() during
 *                                       initialization (default: 1024)
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <dlfcn.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#ifndef TRACEHEAP_NO_PTHREAD
#include <pthread.h>
#endif

// Hooked functions
typedef void *(*malloc_t)(size_t size);
typedef void *(*calloc_t)(size_t count, size_t size);
typedef void *(*realloc_t)(void *ptr, size_t size);
typedef int (*posix_memalign_t)(void **memptr, size_t alignment, size_t size);
typedef void *(*memalign_t)(size_t alignment, size_t size);
typedef void *(*aligned_alloc_t)(size_t alignment, size_t size);
typedef void *(*valloc_t)(size_t size);
typedef void *(*pvalloc_t)(size_t size);
typedef void (*free_t)(void *ptr);
typedef int (*brk_t)(void *addr);
typedef void *(*sbrk_t)(intptr_t increment);

static malloc_t traceheap_base_malloc;
static calloc_t traceheap_base_calloc;
static realloc_t traceheap_base_realloc;
static posix_memalign_t traceheap_base_posix_memalign;
static memalign_t traceheap_base_memalign;
static aligned_alloc_t traceheap_base_aligned_alloc;
static valloc_t traceheap_base_valloc;
static pvalloc_t traceheap_base_pvalloc;
static free_t traceheap_base_free;
static brk_t traceheap_base_brk;
static sbrk_t traceheap_base_sbrk;

// Setup
static volatile atomic_bool traceheap_initialized = 0;
static volatile atomic_bool traceheap_is_initializing = 0;
#ifndef TRACEHEAP_EMERGENCY_CALLOC_SPACE
#define TRACEHEAP_EMERGENCY_CALLOC_SPACE 1024
#endif
static size_t traceheap_emergency_calloc_offset = 0;
static char traceheap_emergency_calloc_buffer[TRACEHEAP_EMERGENCY_CALLOC_SPACE] = {0};
#ifndef TRACEHEAP_NO_PTHREAD
static pthread_mutex_t traceheap_initialization_lock = PTHREAD_MUTEX_INITIALIZER;
#endif
#ifndef TRACEHEAP_LOG_FD
#define TRACEHEAP_LOG_FD STDOUT_FILENO
#endif
static int traceheap_stdout = TRACEHEAP_LOG_FD;

static void traceheap_do_initialize_locked()
{
    // Load base functions
    traceheap_base_malloc = (malloc_t) dlsym(RTLD_NEXT, "malloc");
    traceheap_base_calloc = (calloc_t) dlsym(RTLD_NEXT, "calloc");
    traceheap_base_realloc = (realloc_t) dlsym(RTLD_NEXT, "realloc");
    traceheap_base_posix_memalign = (posix_memalign_t) dlsym(RTLD_NEXT, "posix_memalign");
    traceheap_base_memalign = (memalign_t) dlsym(RTLD_NEXT, "memalign");
    traceheap_base_aligned_alloc = (aligned_alloc_t) dlsym(RTLD_NEXT, "aligned_alloc");
    traceheap_base_valloc = (valloc_t) dlsym(RTLD_NEXT, "valloc");
    traceheap_base_pvalloc = (pvalloc_t) dlsym(RTLD_NEXT, "pvalloc");
    traceheap_base_free = (free_t) dlsym(RTLD_NEXT, "free");
    traceheap_base_brk = (brk_t) dlsym(RTLD_NEXT, "brk");
    traceheap_base_sbrk = (sbrk_t) dlsym(RTLD_NEXT, "sbrk");

    // Duplicate stdout in case the program does something weird
    traceheap_stdout = dup(traceheap_stdout);
}

__attribute__((constructor)) static void traceheap_initialize()
{
    if (atomic_load(&traceheap_initialized))
        return;

#ifndef TRACEHEAP_NO_PTHREAD
    // Lock the mutex for "nice" multithreading
    pthread_mutex_lock(&traceheap_initialization_lock);
    if (atomic_load(&traceheap_initialized))
    {
        pthread_mutex_unlock(&traceheap_initialization_lock);
        return;
    }
#endif

    atomic_store(&traceheap_is_initializing, 1);
    traceheap_do_initialize_locked();
    atomic_store(&traceheap_initialized, 1);
    atomic_store(&traceheap_is_initializing, 0);

#ifndef TRACEHEAP_NO_PTHREAD
    pthread_mutex_unlock(&traceheap_initialization_lock);
#endif
}

// Logging
char hex_digit(unsigned value)
{
    if (value < 10)
        return '0' + value;
    else
        return 'a' + (value - 10);
}

size_t write_hex_to(char *buffer, uintmax_t value, size_t min_digits, bool prefix)
{
    char hex_buffer[sizeof(uintmax_t) * 2 + 2];

    size_t used_width = 1; // Print at least one character
    for (size_t index = sizeof(hex_buffer) - 1; index >= 2; --index)
    {
        char digit = hex_digit(value & 0xF);
        hex_buffer[index] = digit;
        value >>= 4;

        if (digit != '0')
            used_width = sizeof(hex_buffer) - index;
    }
    if (used_width < min_digits)
        used_width = min_digits;

    char *start = hex_buffer + sizeof(hex_buffer) - used_width;

    // Add prefix if desired
    if (prefix)
    {
        start -= 2;
        used_width += 2;
        start[0] = '0';
        start[1] = 'x';
    }

    memcpy(buffer, start, used_width);
    return used_width;
}

size_t write_dec_to(char *buffer, uintmax_t value)
{
    // log(10) / log(2) = 3.32... -> factor 4
    char digit_buffer[sizeof(uintmax_t) * 4 + 1];

    size_t used_width = 1;
    for (size_t index = sizeof(digit_buffer) - 1; index >= 1; --index)
    {
        char digit = hex_digit(value % 10);
        digit_buffer[index] = digit;
        value /= 10;

        if (digit != '0')
            used_width = sizeof(digit_buffer) - index;
    }

    char *start = digit_buffer + sizeof(digit_buffer) - used_width;
    memcpy(buffer, start, used_width);
    return used_width;
}

#define LOG_SPACE 128 // Ensure this is big enough for all possible messages...

#define write_str_to(buffer, string) (memcpy(buffer, ("" string), sizeof("" string)), sizeof("" string))
#define write_ptr_to(buffer, pointer) write_hex_to(buffer, (uintmax_t) (pointer), 12, true)
#define write_size_to(buffer, size) write_dec_to(buffer, size)
#define write_align_to(buffer, align) write_hex_to(buffer, align, 2, true)

#define init_log() char _log[LOG_SPACE]; char *_current = _log; do {} while (0)
#define write_dec(value) do { _current += write_dec_to(_current, value); } while (0)
#define write_hex(value, min_width, prefix) do { _current += write_hex_to(_current, value, min_width, prefix); } while (0)
#define write_str(string) do { _current += write_str_to(_current, string); } while (0)
#define write_ptr(ptr) do { _current += write_ptr_to(_current, ptr); } while (0)
#define write_size(size) do { _current += write_size_to(_current, size); } while (0)
#define write_align(align) do { _current += write_align_to(_current, align); } while (0)
#define flush_log() write(traceheap_stdout, _log, _current - _log)

// Intercepting functions


void *malloc(size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("malloc(");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_malloc(size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

void *calloc(size_t nmemb, size_t size)
{
    // NB: dlsym calls this during initialization to get a little bit of memory.
    // In that case, we use the traceheap_emergency_calloc_buffer.
    if (atomic_load(&traceheap_is_initializing))
    {
        init_log();
        write_str("emergency_calloc(");
        write_size(nmemb);
        write_str(", ");
        write_size(size);
        write_str(") [calloc from dlsym()] -> ");

        void *result;
        if (nmemb > TRACEHEAP_EMERGENCY_CALLOC_SPACE || \
            size > TRACEHEAP_EMERGENCY_CALLOC_SPACE || \
            nmemb * size > TRACEHEAP_EMERGENCY_CALLOC_SPACE)
        {
            result = NULL;
        }
        else
        {
            result = &traceheap_emergency_calloc_buffer[traceheap_emergency_calloc_offset];
            traceheap_emergency_calloc_offset += nmemb * size;
        }

        write_ptr(result);
        write_str("\n");
        flush_log();

        return result;
    }
    else
    {
        traceheap_initialize();

        init_log();
        write_str("calloc(");
        write_size(nmemb);
        write_str(", ");
        write_size(size);
        write_str(") -> ");

        void *result = traceheap_base_calloc(nmemb, size);

        write_ptr(result);
        write_str("\n");
        flush_log();

        return result;
    }
}

void *realloc(void *ptr, size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("realloc(");
    write_ptr(ptr);
    write_str(", ");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_realloc(ptr, size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

int posix_memalign(void **memptr, size_t alignment, size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("posix_memalign(");
    write_ptr(memptr);
    write_str(", ");
    write_align(alignment);
    write_str(", ");
    write_size(size);
    write_str(") -> ");

    int result = traceheap_base_posix_memalign(memptr, alignment, size);

    write_size(size);
#ifndef TRACEHEAP_NO_POSIX_MEMALIGN_RESULT
    if (result == 0)
    {
        write_str(" at ");
        write_ptr(*memptr);
    }
#endif
    write_str("\n");
    flush_log();

    return result;
}

void *memalign(size_t alignment, size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("memalign(");
    write_align(alignment);
    write_str(", ");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_memalign(alignment, size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

void *aligned_alloc(size_t alignment, size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("aligned_alloc(");
    write_align(alignment);
    write_str(", ");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_aligned_alloc(alignment, size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

void *valloc(size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("valloc(");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_valloc(size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

void *pvalloc(size_t size)
{
    traceheap_initialize();

    init_log();
    write_str("pvalloc(");
    write_size(size);
    write_str(") -> ");

    void *result = traceheap_base_pvalloc(size);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}

void free(void *ptr)
{
    traceheap_initialize();

    init_log();
    write_str("free(");
    write_ptr(ptr);
    write_str(")\n");
    flush_log();

    traceheap_base_free(ptr);
}

int brk(void *addr)
{
    traceheap_initialize();

    init_log();
    write_str("brk(");
    write_ptr(addr);
    write_str(") -> ");

    int result = traceheap_base_brk(addr);

    write_dec(result);
    write_str("\n");
    flush_log();

    return result;
}

void *sbrk(intptr_t increment)
{
    traceheap_initialize();

    init_log();
    write_str("sbrk(");
    write_hex(increment, 4, true);
    write_str(") -> ");

    void *result = traceheap_base_sbrk(increment);

    write_ptr(result);
    write_str("\n");
    flush_log();

    return result;
}
